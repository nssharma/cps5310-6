function P = logistic(p,t);
%LOGISTIC: MATLAB function file that takes
%time t, growth rate r (p(1)),
%carrying capacity K (p(2)),
%and initial population P0 (p(3)), and returns
%the population at time t.
%considering a=1
P = p(2).*p(3)./((p(2)-p(3)).*exp(-p(1).*t)+p(3));
